#include"Cluster_test.h"
#include "SegDispWrite.h"

bool StartSpeedTest_b = TRUE;
bool StartODOmeterTest_b = FALSE;
static uint8_t j=4;




typedef struct
{
	ClusterSignals_En_t	SIGNAL_E;
	Func_pointer		test_siganl_fptr;
}Test_Case_conf_st_t;


Test_Case_conf_st_t	Test_Case_conf_ast[TOTALSIGTEST]=
{
	{COMMON_SIG_E,		Telltale_Test	},
	{TOP_TEXT_E,		Telltale_Test	},
	{SPEED_E,		SpeedSignalTest	},
	{ODO_E,			ODO_Sig_Test	},
	{NAVIG_DIST_E,		Navig_dist_Test	},
	{LEFT_IND_E,		Telltale_Test	},
	{RIGHT_IND_E,		Telltale_Test	},
	{WARNING_IND_E,		Telltale_Test	},
	{KILL_SWITCH_E,		Telltale_Test	},
	{MOTOR_FAULT_E,		Telltale_Test	},
	{SERV_REM_E,		Telltale_Test	},
	{ENGINE_FAULT_E,	Telltale_Test	},
	{SIDE_STAND_E,		Telltale_Test	},
	{NETWORK_E,		Telltale_Test	},
	{BLE_E,			Telltale_Test	},
	{HIGH_BEAM_E,		Telltale_Test	},
	{ODO_TEXT_E,		Telltale_Test	},
	{TRIP_TEXT_E,		Telltale_Test	},
	{TEXT_A_E,		Telltale_Test	},
	{TEXT_B_E,		Telltale_Test	},
	{CHARGING_STATUS_E,	Telltale_Test	},
	{POWER_W_IND_E,		Telltale_Test	},
	{NEUTRAL_MODE_E,	Telltale_Test	},
	{ECO_MODE_E,		Telltale_Test	},
	{SPORTS_MODE_E,		Telltale_Test	},
	{REVERSE_MODE_E,	Telltale_Test	},
	{BATT_SOC_BAR_E,	batt_bar_test	},
	{POWER_CONSUMP_E,	batt_bar_test	},
	{RANGE_KM_E,		Range_Test	},
	{BATT_SOC_DIGIT_E,	batt_Per_Test	},
	{TIME_COLON_E,		Telltale_Test	},
	{TEXT_AM_E, 		Telltale_Test	},
	{TEXT_PM_E,		Telltale_Test	},		
	{MINUTES_TIME_E,	Time_Minute	},
	{HOURS_TIME_E,		Time_Hour	},
	{NAVIGATION_DIR_E,	Navig_Dir_Test	},
	
};


void TestMain(void)
{
	
	if(j<TOTALSIGTEST)
	{
		Test_Case_conf_ast[j].test_siganl_fptr(Test_Case_conf_ast[j].SIGNAL_E);
		
	}
	
	

}

void SpeedSignalTest(ClusterSignals_En_t SIGNAL_E)
{
	static uint8_t SpeedValue_u8 = 0;
	static uint8_t i=1;
	
	
	Write_SEG(SIGNAL_E, (uint32_t)SpeedValue_u8);
	
	SpeedValue_u8+=i;
	if(SpeedValue_u8>9)
	{
		i=10;
	}
		
	
	if(SpeedValue_u8 > 210)
	{
		j++;
	}
	
	return;
}


void ODO_Sig_Test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t ODO_Value_u32 = 0;
	static uint32_t i=1,k=0;
	uint32_t arr_au32[2]={0x0f,0};
	 
	
	Write_SEG(SIGNAL_E, ODO_Value_u32);
	ODO_Value_u32+=i;
	
	switch(ODO_Value_u32)
	{
		case 10: 		i=10;
			   		break;
				
				
		case 100 :		i=100;
			   		break;
				
				
		case 1000:		i=1000;
					break;
				
				
		case 10000: 		i=10000;
					break;
				
				
		case 100000: 		i=100000;
					break;
					
		}
	if(ODO_Value_u32 > 1000000)
	{
		
		Write_SEG(SIGNAL_E, arr_au32[k]);
		k++;
		if(2==k)
		{
			j++;
		}
	}
	
}




void Navig_dist_Test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t Navig_Value_u32=0;
	static uint32_t i_navig=1,k=0;
	uint32_t arr_au32[2]={0,0XFFF};
	
	Write_SEG(SIGNAL_E, Navig_Value_u32);
	
	Navig_Value_u32+=i_navig;
	if(Navig_Value_u32 == 10)
	{
		i_navig=10;
	}
	if(Navig_Value_u32 > 90)
	{
		i_navig=100;
	}
	//j++;
	
	
	if(Navig_Value_u32 >= 900)
	{
		
		Write_SEG(SIGNAL_E, arr_au32[k]);
		k++;
		if(2==k)
		{
			j++;
		}
	}
	
}

void Trip_text_Test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_trptxt=0;
	uint32_t arr[3]={111,0,111};
	Write_SEG(SIGNAL_E, arr[i_trptxt]);
	i_trptxt++;
	
	if(i_trptxt==3)
	{
		j++;
	}	
}


void Telltale_Test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_Telltale=0;
	uint32_t arr[3]={0x01,0x00,0x01};
	Write_SEG(SIGNAL_E, arr[i_Telltale]);
	i_Telltale++;
	
	if(i_Telltale==3)
	{
		j++;
		i_Telltale=0;
	}
}
void batt_bar_test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_bar=0;
	uint32_t arr[7]={1,11,111,1111,11111,0,11111};
	Write_SEG(SIGNAL_E, arr[i_bar++]);
	
	
	if(i_bar==7)
	{
		j++;
		i_bar=0;
	}
}

void Range_Test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_range_u32=1,k_u32=0;
	static uint32_t Range_value_u32=0;
	uint32_t arr_au32[2]={0x0f,0};
	Write_SEG(SIGNAL_E, Range_value_u32);
	Range_value_u32+=i_range_u32;
	switch(Range_value_u32)
	{
		case 10:	i_range_u32=10;
				break;
		case 100:	i_range_u32=100;
				break;
	}
	
	if(Range_value_u32 > 1000)
	{
		
		Write_SEG(SIGNAL_E, arr_au32[k_u32]);
		k_u32++;
		if(2==k_u32)
		{
			j++;
		}
	}
}

void batt_Per_Test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_per_u32=1,k_per_u32=0;
	static uint32_t Percen_u32=0;
	uint32_t arr_au32[2]={0x0f,0};
	Write_SEG(SIGNAL_E, Percen_u32);
	Percen_u32+=i_per_u32;
	switch(Percen_u32)
	{
		case 10:	i_per_u32=10;
				break;
	}
	
	if(Percen_u32 > 110)
	{
		
		Write_SEG(SIGNAL_E, arr_au32[k_per_u32]);
		k_per_u32++;
		if(2==k_per_u32)
		{
			j++;
		}	
	}
	
}

void Time_Meridiem(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t k_am_u32=0;
	uint32_t arr_au32[4]={1,10,0,11};
	Write_SEG(SIGNAL_E, arr_au32[k_am_u32++]);
	if(k_am_u32 == 4)
	{
		j++;
	}
	
	
}
void Time_Minute(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_min_u32=1,k_min_u32=0;
	static uint32_t minute_u32=0;
	uint32_t arr_au32[2]={0x0f,0};
	Write_SEG(SIGNAL_E, minute_u32);
	minute_u32+=i_min_u32;
	
	switch(minute_u32)
	{
		case 10:	i_min_u32=10;
				break;
	}
	
	
	if(minute_u32 > 90)
	{
		
		Write_SEG(SIGNAL_E, arr_au32[k_min_u32]);
		k_min_u32++;
		if(2==k_min_u32)
		{
			j++;
		}	
	}
	
}
void Time_Hour(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_hour_u32=1,k_hour_u32=0;
	static uint32_t hour_u32=0;
	uint32_t arr_au32[2]={0x0f,0};
	Write_SEG(SIGNAL_E, hour_u32);
	hour_u32+=i_hour_u32;
	
	
	if(hour_u32 > 10)
	{
		
		Write_SEG(SIGNAL_E, arr_au32[k_hour_u32]);
		k_hour_u32++;
		if(2==k_hour_u32)
		{
			j++;
		}	
	}
}


void Navig_Dir_Test(ClusterSignals_En_t SIGNAL_E)
{
	static uint32_t i_Dir_u32=0;
	uint32_t arr_au32[7]={1,10,100,1000,10000,0,11111};
	Write_SEG(SIGNAL_E, arr_au32[i_Dir_u32++]);
	if(7 == i_Dir_u32)
	{
		j++;
	}
	
	
}


