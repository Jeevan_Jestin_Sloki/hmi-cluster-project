/***********************************************************************************************************************
* File Name    : ODO_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 21/10/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ODO_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/

/*  		ODO text Configration*/
 SignalsValue_St_t			ODO_TEXT_SignalsValue_aSt[ODO_TEXT_SIG_LEN]=
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	ZERO_E,		TWO_E,		ODO_Text_sigconf_ast
};

 SignalConfig_St_t		ODO_Text_sigconf_ast[TWO_E]=
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ON_E,		ONE_SEG_E, 	ODO_Texton_SegConf_ast},
	{OFF_E,		ONE_SEG_E, 	ODO_Textoff_SegConf_ast},
};

 SegConfig_St_t		ODO_Texton_SegConf_ast[ONE_SEG_E]=
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	&SEG17,		0x0EU, 		0x01U
};

 SegConfig_St_t		ODO_Textoff_SegConf_ast[ONE_SEG_E]=
{
	&SEG17,		0x0EU, 		0x00U
};

/*
					ODO text Configration end
*/




/* 		
			TRIP Text configration
*/
 SignalsValue_St_t			TRIP_Text_SignalsValue_aSt[TRIP_TEXT_SIG_LEN]=
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ZERO_E, 	TWO_E,		Trip_Text_SigConf_ast	},
};

 SignalConfig_St_t				Trip_Text_SigConf_ast[TWO_E]=
{
	{ON_E, 		ONE_SEG_E, 	Trip_Texton_SegConf_ast},
	{OFF_E,		ONE_SEG_E, 	Trip_Textoff_SegConf_ast},
};

 SegConfig_St_t				Trip_Texton_SegConf_ast[ONE_SEG_E]=
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	&SEG19,		0x0EU, 		0x01U
};

 SegConfig_St_t				Trip_Textoff_SegConf_ast[ONE_SEG_E]=
{
	&SEG19,		0x0EU, 		0x00U
};

/*
			Text A configration
*/

SignalsValue_St_t			Text_A_SignalsValue_aSt[TEXT_A_SIG_LEN]=
{
	{ZERO_E,	TWO_E, 		Text_A_SigConf_ast},
};

SignalConfig_St_t				Text_A_SigConf_ast[TWO_E]=
{
	{ON_E, 		ONE_SEG_E, 	Text_Aon_SegConf_ast },
	{OFF_E, 	ONE_SEG_E, 	Text_Aoff_SegConf_ast},
};

SegConfig_St_t				Text_Aon_SegConf_ast[ONE_SEG_E]=
{
	&SEG21,		0x0EU, 		0x01U
};
 SegConfig_St_t				Text_Aoff_SegConf_ast[ONE_SEG_E]=
{
	&SEG21,		0x0EU, 		0x00U
};


/*
			Text B configration
*/


SignalsValue_St_t			Text_B_SignalsValue_aSt[TEXT_B_SIG_LEN]=
{
	{ZERO_E,	TWO_E, 		Text_B_SigConf_ast},
};

 SignalConfig_St_t				Text_B_SigConf_ast[TWO_E]=
{
	{ON_E, 		ONE_SEG_E, 	Text_Bon_SegConf_ast },
	{OFF_E, 	ONE_SEG_E, 	Text_Boff_SegConf_ast},
};

SegConfig_St_t				Text_Bon_SegConf_ast[ONE_SEG_E]=
{
	&SEG25,		0x0EU, 		0x01U
};
 SegConfig_St_t				Text_Boff_SegConf_ast[ONE_SEG_E]=
{
	&SEG25,		0x0EU, 		0x00U
};

/*
				ODO Digits configration
*/



 SignalsValue_St_t			ODO_SignalsValue_aSt[ODO_SIG_LEN] =
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ZERO_E, 	ELEVEN_E,	ODOSig1Conf_ast},
	{ONE_E, 	ELEVEN_E,	ODOSig2Conf_ast},
	{TWO_E, 	ELEVEN_E,	ODOSig3Conf_ast},
	{THREE_E,	ELEVEN_E,	ODOSig4Conf_ast},
	{FOUR_E, 	ELEVEN_E,	ODOSig5Conf_ast},
	{FIVE_E, 	ELEVEN_E,	ODOSig6Conf_ast},
	
};


 SignalConfig_St_t			ODOSig1Conf_ast[ELEVEN_E] = 
{
	{ZERO_E,	TWO_SEG_E,	ODO_S1_0_SegConf_aSt },
	{ONE_E,		TWO_SEG_E,	ODO_S1_1_SegConf_aSt },
	{TWO_E,		TWO_SEG_E,	ODO_S1_2_SegConf_aSt },
	{THREE_E,	TWO_SEG_E,	ODO_S1_3_SegConf_aSt },
	{FOUR_E,	TWO_SEG_E,	ODO_S1_4_SegConf_aSt },
	{FIVE_E,	TWO_SEG_E,	ODO_S1_5_SegConf_aSt },
	{SIX_E,		TWO_SEG_E,	ODO_S1_6_SegConf_aSt },
	{SEVEN_E,	TWO_SEG_E,	ODO_S1_7_SegConf_aSt },
	{EIGHT_E,	TWO_SEG_E,	ODO_S1_8_SegConf_aSt },
	{NINE_E,	TWO_SEG_E,	ODO_S1_9_SegConf_aSt },
	{OFF_DIGIT,	TWO_SEG_E,	ODO_S1_off_SegConf_aSt},
};


 SignalConfig_St_t			ODOSig2Conf_ast[ELEVEN_E] = 
{
	{ZERO_E,	TWO_SEG_E,	ODO_S2_0_SegConf_aSt},
	{ONE_E,		TWO_SEG_E,	ODO_S2_1_SegConf_aSt},
	{TWO_E,		TWO_SEG_E,	ODO_S2_2_SegConf_aSt},
	{THREE_E,	TWO_SEG_E,	ODO_S2_3_SegConf_aSt},
	{FOUR_E,	TWO_SEG_E,	ODO_S2_4_SegConf_aSt},
	{FIVE_E,	TWO_SEG_E,	ODO_S2_5_SegConf_aSt},
	{SIX_E,		TWO_SEG_E,	ODO_S2_6_SegConf_aSt},
	{SEVEN_E,	TWO_SEG_E,	ODO_S2_7_SegConf_aSt},
	{EIGHT_E,	TWO_SEG_E,	ODO_S2_8_SegConf_aSt},
	{NINE_E,	TWO_SEG_E,	ODO_S2_9_SegConf_aSt},
	{OFF_DIGIT,	TWO_SEG_E,	ODO_S2_off_SegConf_aSt},
};


 SignalConfig_St_t			ODOSig3Conf_ast[ELEVEN_E] = 
{
	{ZERO_E,	TWO_SEG_E,	ODO_S3_0_SegConf_aSt},
	{ONE_E,		TWO_SEG_E,	ODO_S3_1_SegConf_aSt},
	{TWO_E,		TWO_SEG_E,	ODO_S3_2_SegConf_aSt},
	{THREE_E,	TWO_SEG_E,	ODO_S3_3_SegConf_aSt},
	{FOUR_E,	TWO_SEG_E,	ODO_S3_4_SegConf_aSt},
	{FIVE_E,	TWO_SEG_E,	ODO_S3_5_SegConf_aSt},
	{SIX_E,		TWO_SEG_E,	ODO_S3_6_SegConf_aSt},
	{SEVEN_E,	TWO_SEG_E,	ODO_S3_7_SegConf_aSt},
	{EIGHT_E,	TWO_SEG_E,	ODO_S3_8_SegConf_aSt},
	{NINE_E,	TWO_SEG_E,	ODO_S3_9_SegConf_aSt},
	{OFF_DIGIT,	TWO_SEG_E,	ODO_S3_off_SegConf_aSt},
};


 SignalConfig_St_t			ODOSig4Conf_ast[ELEVEN_E] = 
{
	{ZERO_E,	TWO_SEG_E,	ODO_S4_0_SegConf_aSt},
	{ONE_E,		TWO_SEG_E,	ODO_S4_1_SegConf_aSt},
	{TWO_E,		TWO_SEG_E,	ODO_S4_2_SegConf_aSt},
	{THREE_E,	TWO_SEG_E,	ODO_S4_3_SegConf_aSt},
	{FOUR_E,	TWO_SEG_E,	ODO_S4_4_SegConf_aSt},
	{FIVE_E,	TWO_SEG_E,	ODO_S4_5_SegConf_aSt},
	{SIX_E,		TWO_SEG_E,	ODO_S4_6_SegConf_aSt},
	{SEVEN_E,	TWO_SEG_E,	ODO_S4_7_SegConf_aSt},
	{EIGHT_E,	TWO_SEG_E,	ODO_S4_8_SegConf_aSt},
	{NINE_E,	TWO_SEG_E,	ODO_S4_9_SegConf_aSt},
	{OFF_DIGIT,	TWO_SEG_E,	ODO_S4_off_SegConf_aSt},
};


 SignalConfig_St_t			ODOSig5Conf_ast[ELEVEN_E] = 
{
	{ZERO_E,	TWO_SEG_E,	ODO_S5_0_SegConf_aSt},
	{ONE_E,		TWO_SEG_E,	ODO_S5_1_SegConf_aSt},
	{TWO_E,		TWO_SEG_E,	ODO_S5_2_SegConf_aSt},
	{THREE_E,	TWO_SEG_E,	ODO_S5_3_SegConf_aSt},
	{FOUR_E,	TWO_SEG_E,	ODO_S5_4_SegConf_aSt},
	{FIVE_E,	TWO_SEG_E,	ODO_S5_5_SegConf_aSt},
	{SIX_E,		TWO_SEG_E,	ODO_S5_6_SegConf_aSt},
	{SEVEN_E,	TWO_SEG_E,	ODO_S5_7_SegConf_aSt},
	{EIGHT_E,	TWO_SEG_E,	ODO_S5_8_SegConf_aSt,},
	{NINE_E,	TWO_SEG_E,	ODO_S5_9_SegConf_aSt},
	{OFF_DIGIT,	TWO_SEG_E,	ODO_S5_off_SegConf_aSt},
};


 SignalConfig_St_t			ODOSig6Conf_ast[ELEVEN_E] = 
{
	{ZERO_E,	TWO_SEG_E,	ODO_S6_0_SegConf_aSt},
	{ONE_E,		TWO_SEG_E,	ODO_S6_1_SegConf_aSt,},
	{TWO_E,		TWO_SEG_E,	ODO_S6_2_SegConf_aSt},
	{THREE_E,	TWO_SEG_E,	ODO_S6_3_SegConf_aSt},
	{FOUR_E,	TWO_SEG_E,	ODO_S6_4_SegConf_aSt},
	{FIVE_E,	TWO_SEG_E,	ODO_S6_5_SegConf_aSt},
	{SIX_E,		TWO_SEG_E,	ODO_S6_6_SegConf_aSt},
	{SEVEN_E,	TWO_SEG_E,	ODO_S6_7_SegConf_aSt},
	{EIGHT_E,	TWO_SEG_E,	ODO_S6_8_SegConf_aSt},
	{NINE_E,	TWO_SEG_E,	ODO_S6_9_SegConf_aSt},
	{OFF_DIGIT,	TWO_SEG_E,	ODO_S6_off_SegConf_aSt},
};


 SegConfig_St_t			ODO_S1_0_SegConf_aSt[TWO_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{&SEG29,	0x01U,		0x0AU},
	{&SEG28,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S1_1_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0AU},
	{&SEG28,	0x00U,		0x00U},
};

 SegConfig_St_t			ODO_S1_2_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x06U},
	{&SEG28,	0x00U,		0x0DU},
};

 SegConfig_St_t			ODO_S1_3_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0EU},
	{&SEG28,	0x00U,		0x09U},
};

 SegConfig_St_t			ODO_S1_4_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0EU},
	{&SEG28,	0x00U,		0x02U},
};

 SegConfig_St_t			ODO_S1_5_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0CU},
	{&SEG28,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S1_6_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0CU},
	{&SEG28,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S1_7_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0AU},
	{&SEG28,	0x00U,		0x01U},
};

 SegConfig_St_t			ODO_S1_8_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0EU},
	{&SEG28,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S1_9_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x0EU},
	{&SEG28,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S1_off_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG29,	0x01U,		0x00U},
	{&SEG28,	0x00U,		0x00U},
};



 SegConfig_St_t			ODO_S2_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0AU},
	{&SEG26,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S2_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0AU},
	{&SEG26,	0x00U,		0x00U},
};

 SegConfig_St_t			ODO_S2_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x06U},
	{&SEG26,	0x00U,		0x0DU},
};

 SegConfig_St_t			ODO_S2_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0EU},
	{&SEG26,	0x00U,		0x09U},
};

 SegConfig_St_t			ODO_S2_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0EU},
	{&SEG26,	0x00U,		0x02U},
};

 SegConfig_St_t			ODO_S2_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0CU},
	{&SEG26,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S2_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0CU},
	{&SEG26,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S2_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0AU},
	{&SEG26,	0x00U,		0x01U},
};

 SegConfig_St_t			ODO_S2_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0EU},
	{&SEG26,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S2_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x0EU},
	{&SEG26,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S2_off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG27,	0x01U,		0x00U},
	{&SEG26,	0x00U,		0x00U},
};



 SegConfig_St_t			ODO_S3_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0AU},
	{&SEG22,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S3_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0AU},
	{&SEG22,	0x00U,		0x00U},
};

 SegConfig_St_t			ODO_S3_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x06U},
	{&SEG22,	0x00U,		0x0DU},
};

 SegConfig_St_t			ODO_S3_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0EU},
	{&SEG22,	0x00U,		0x09U},
};

 SegConfig_St_t			ODO_S3_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0EU},
	{&SEG22,	0x00U,		0x02U},
};

 SegConfig_St_t			ODO_S3_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0CU},
	{&SEG22,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S3_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0CU},
	{&SEG22,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S3_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0AU},
	{&SEG22,	0x00U,		0x01U},
};

 SegConfig_St_t			ODO_S3_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0EU},
	{&SEG22,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S3_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x0EU},
	{&SEG22,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S3_off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG25,	0x01U,		0x00U},
	{&SEG22,	0x00U,		0x00U},
};



 SegConfig_St_t			ODO_S4_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0AU},
	{&SEG20,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S4_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0AU},
	{&SEG20,	0x00U,		0x00U},
};

 SegConfig_St_t			ODO_S4_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x06U},
	{&SEG20,	0x00U,		0x0DU},
};

 SegConfig_St_t			ODO_S4_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0EU},
	{&SEG20,	0x00U,		0x09U},
};

 SegConfig_St_t			ODO_S4_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0EU},
	{&SEG20,	0x00U,		0x02U},
};

 SegConfig_St_t			ODO_S4_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0CU},
	{&SEG20,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S4_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0CU},
	{&SEG20,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S4_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0AU},
	{&SEG20,	0x00U,		0x01U},
};

 SegConfig_St_t			ODO_S4_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0EU},
	{&SEG20,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S4_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x0EU},
	{&SEG20,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S4_off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG21,	0x01U,		0x00U},
	{&SEG20,	0x00U,		0x00U},
};



 SegConfig_St_t			ODO_S5_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x0AU},
	{&SEG18,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S5_1_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG19,	0x01U,		0x0AU},
	{&SEG18,	0x00U,		0x00U},
};

 SegConfig_St_t			ODO_S5_2_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG19,	0x01U,		0x06U},
	{&SEG18,	0x00U,		0x0DU},
};

 SegConfig_St_t			ODO_S5_3_SegConf_aSt[TWO_SEG_E] = 
{
	
	{&SEG19,	0x01U,		0x0EU},
	{&SEG18,	0x00U,		0x09U},
};

 SegConfig_St_t			ODO_S5_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x0EU},
	{&SEG18,	0x00U,		0x02U},
};

 SegConfig_St_t			ODO_S5_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x0CU},
	{&SEG18,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S5_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x0CU},
	{&SEG18,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S5_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x0AU},
	{&SEG18,	0x00U,		0x01U},
};

 SegConfig_St_t			ODO_S5_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x0EU},
	{&SEG18,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S5_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x0EU},
	{&SEG18,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S5_off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG19,	0x01U,		0x00U},
	{&SEG18,	0x00U,		0x00U},
};



 SegConfig_St_t			ODO_S6_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0AU},
	{&SEG16,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S6_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0AU},
	{&SEG16,	0x00U,		0x00U},
};

 SegConfig_St_t			ODO_S6_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x06U},
	{&SEG16,	0x00U,		0x0DU},
};

 SegConfig_St_t			ODO_S6_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0EU},
	{&SEG16,	0x00U,		0x09U},
};

 SegConfig_St_t			ODO_S6_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0EU},
	{&SEG16,	0x00U,		0x02U},
};

 SegConfig_St_t			ODO_S6_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0CU},
	{&SEG16,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S6_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0CU},
	{&SEG16,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S6_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0AU},
	{&SEG16,	0x00U,		0x01U},
};

 SegConfig_St_t			ODO_S6_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0EU},
	{&SEG16,	0x00U,		0x0FU},
};

 SegConfig_St_t			ODO_S6_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x0EU},
	{&SEG16,	0x00U,		0x0BU},
};

 SegConfig_St_t			ODO_S6_off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG17,	0x01U,		0x00U},
	{&SEG16,	0x00U,		0x00U},
};


/*
				ODO Digits  configration end
*/

/********************************************************EOF***********************************************************/