/***********************************************************************************************************************
* File Name    : GenConfig.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 18/12/2020
***********************************************************************************************************************/

#ifndef GEN_CONFIG_H
#define GEN_CONFIG_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h" 

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
/*
	HMI Cluster Signals Length Configuration.
*/

#define         COMMON_SEG_SIG_LEN			0x01
#define		SPEED_SIG_LEN				0x03
#define		RANGE_SIG_LEN				0x03
#define 	LEFT_IND_SIG_LEN			0x01
#define 	RIGHT_IND_SIG_LEN			0x01
#define 	HIGH_BEAM_SIG_LEN			0x01
#define		BLE_SIG_LEN				0x01
#define 	WARN_IND_SIG_LEN			0x01
#define		SERV_REM_SIG_LEN			0x01
#define		NEUTRAL_MODE_SIG_LEN			0x01
#define		ECO_MODE_SIG_LEN			0x01
#define		SPORTS_MODE_SIG_LEN			0x01
#define		REVERSE_MODE_SIG_LEN			0x01
#define		SIDE_STAND_SIG_LEN			0x01
#define		KILL_SWITCH_SIG_LEN			0x01
#define 	BATT_SOC_BAR_SIG_LEN			0x05
#define 	BATT_SOC_DIGIT_SIG_LEN			0x03
#define		POWER_CONSUMP_SIG_LEN			0x05
#define		ODO_SIG_LEN				0x06
#define		MINUTES_TIME_SIG_LEN			0x02
#define		HOURS_TIME_SIG_LEN			0x02
#define 	TEXT_AM_SIG_LEN				0x01
#define 	TEXT_PM_SIG_LEN				0x01
#define         TIME_COLON_SIG_LEN			0x01
#define		MOTOR_FLT_SIG_LEN			0x01
#define 	NAVIG_DIR_SIG_LEN			0x05
#define 	NAVIG_DIST_SIG_LEN			0x03
#define		ENGINE_FLT_SIG_LEN			0x01
#define		NETWORK_SIG_LEN		         	0x01
#define 	ODO_TEXT_SIG_LEN			0x01
#define 	TRIP_TEXT_SIG_LEN			0x01
#define		TEXT_A_SIG_LEN				0x01
#define 	TEXT_B_SIG_LEN				0x01
#define 	POWER_IND_SIG_LEN			0x01
#define 	TOP_TEXT_SIG_LEN_E			0x01
#define 	CH_STATUS_SIG_LEN_E			0x01
#define 	MAX_LENGTH_SIGNAL		     ODO_SIG_LEN 



//#define 	MIN_SPEED_RANGE				0
//#define		MAX_SPEED_RANGE				199

//#define 	MIN_MILEAGE_RANGE			0
//#define		MAX_MILEAGE_RANGE			99

//#define 	MIN_RANGE_KM_RANGE			0
//#define		MAX_RANGE_KM_RANGE			999

//#define         MIN_ODO_RANGE_IN_METER                  0
//#define         MAX_ODO_RANGE_IN_METER                  99999900

//#define		MIN_SOC_RANGE				0
//#define 	MAX_SOC_RANGE				100
//#define		EACH_BAR_SOC_RANGE			10

//#define		MIN_POWER_CONSUMP_RANGE			0
//#define 	MAX_POWER_CONSUMP_RANGE			5000
//#define		EACH_BAR_POWER_CONSUMP_RANGE		500
//#define 	MAX_PWR_CONSUM_BARS			10

//#define         MAX_MINUTES_TIME			59
//#define		MAX_HOURS_TIME				23


//#define         _23_UNUSED_SEGMENT		        23
//#define         _24_UNUSED_SEGMENT		        24
//#define         _44_UNUSED_SEGMENT		        44

//#define         CLEAR_SEG_REGISTER			0x00U
//#define         SET_SEG_REGISTER			0x0FU


///*Telltale signals value configuration*/
//#define TURN_ON						0x03U
//#define TURN_OFF					0x00U
//#define BLINK						0x01U
//#define HOLD 						0x02U

#define TOTAL_SEG_REGISTERS				48


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/


#endif /* GEN_CONFIG_H */


