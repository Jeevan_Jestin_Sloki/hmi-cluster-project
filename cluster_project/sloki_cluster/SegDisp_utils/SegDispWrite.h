/***********************************************************************************************************************
* File Name    : SegDispWrite.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 25/12/2020
***********************************************************************************************************************/

#ifndef SEG_DISP_WRITE_H
#define SEG_DISP_WRITE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "Cluster_Conf.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern void Write_SEG(ClusterSignals_En_t ClusterSignals_En, uint32_t Signal_Value_u32);

#endif /* SEG_DISP_WRITE_H */


