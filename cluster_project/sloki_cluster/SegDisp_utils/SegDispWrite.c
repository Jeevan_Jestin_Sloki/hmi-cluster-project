/***********************************************************************************************************************
* File Name    : SegDispWrite.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 25/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SegDispWrite.h"
#include "digits_utils.h"
#include "GenConfig.h"


/***********************************************************************************************************************
macro directive
***********************************************************************************************************************/
#define     REG_WRITE(i,j,k,l)   	(*(ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Seg_pu8) =  \
					(*(ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Seg_pu8) &  \
					ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Mask_u8) | 	  \
					ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Value_u8)	

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

uint8_t 	SignalVal_au8[MAX_LENGTH_SIGNAL] = {0x0F,0x0F,0x0F,0x0F,0x0F,0x0F};
ClusterSignals_En_t	tempenum;


/***********************************************************************************************************************
* Function Name: Write_SEG
* Description  : This function Writes Data into the Segment registers to display the signals on the cluster.
* Arguments    : ClusterSignals_En_t ClusterSignals_En, uint32_t Signal_Value_u32
* Return Value : None
***********************************************************************************************************************/
void Write_SEG(ClusterSignals_En_t ClusterSignals_En, uint32_t Signal_Value_u32)
{
	uint16_t	i = 0; 
	uint16_t	j = 0;
	uint16_t	k = 0;
	uint16_t	l = 0;
	uint8_t 	SignalVal_au8[MAX_LENGTH_SIGNAL] = {0x0F,0x0F,0x0F,0x0F,0x0F,0x0F};
	
	tempenum = ClusterSignals_En;
	
		if(9 < Signal_Value_u32)
		{
			GetDigitsFromNum(Signal_Value_u32,SignalVal_au8);	
		}
		else
		{
			SignalVal_au8[0] = (uint8_t)Signal_Value_u32;	
		}
/* 				user code begin			*/		
	if(ClusterSignals_En == SPEED_E)
	{
		if(Signal_Value_u32 == 200)
		{
			SignalVal_au8[0] = 0x0f;//0x00U;
			SignalVal_au8[1] = 0x0f;//0x0FU;
			SignalVal_au8[2] = 0x0FU;
		}
		if(Signal_Value_u32 > 200)
		{
			SignalVal_au8[0] = 0;//0x00U;
			SignalVal_au8[1] = 'G';//0x0FU;
			SignalVal_au8[2] = 0x0FU;
		}
	}
	
	if(ClusterSignals_En == NAVIG_DIST_E)
	{
		
		if(Signal_Value_u32 == 0)
		{
			SignalVal_au8[2] = 0;//0x00U;
			SignalVal_au8[1] = 0x01;//0x0FU;
			SignalVal_au8[0] = 0x0F;
		}
		else if((Signal_Value_u32 >= 10)&&(Signal_Value_u32 < 100))
		{
			SignalVal_au8[2] = 0;//0x00U;
			SignalVal_au8[1] = 0x01;//0x0FU;
			
		}
		
		else if((Signal_Value_u32 >= 100)&&(Signal_Value_u32 < 1000))
		{
			SignalVal_au8[1] = 0x01;
			SignalVal_au8[2] = 0x01;
		}
		if(Signal_Value_u32 == 0XFFF)
		{
			SignalVal_au8[2] = 0;//0x00U;
			SignalVal_au8[1] = 0x01;//0x0FU;
			SignalVal_au8[0] = 0x0F;
		}
	}
	if((ClusterSignals_En == ODO_E)|| (ClusterSignals_En == RANGE_KM_E) || (ClusterSignals_En == BATT_SOC_DIGIT_E)  )
	{
		if(Signal_Value_u32==15)
		{
			SignalVal_au8[0] = 0x0F;
			SignalVal_au8[1] = 0x0F;
		}
	}
	
	if(ClusterSignals_En == TEXT_AM_E || ClusterSignals_En == MINUTES_TIME_E)
	{
		if(0 == Signal_Value_u32)
		{
			SignalVal_au8[0] = 0;
			SignalVal_au8[1] = 0;
			
		}
	}
	if(ClusterSignals_En == MINUTES_TIME_E)
	{
		if(15 == Signal_Value_u32)
		{
			SignalVal_au8[0] = 0x0f;
			SignalVal_au8[1] = 0x0f;
			
		}
	}
	if(ClusterSignals_En == HOURS_TIME_E)
	{
		if(15 == Signal_Value_u32)
		{
			SignalVal_au8[0] = 0x0f;
			SignalVal_au8[1] = 0;
			
		}
		else if(0 == Signal_Value_u32)
		{
			SignalVal_au8[0] = 0;
			SignalVal_au8[1] = 1;	
		}
	}
	if(ClusterSignals_En == NAVIGATION_DIR_E)
	{
		
		if(0 == Signal_Value_u32)
		{
			SignalVal_au8[0] = 0;
			SignalVal_au8[1] = 0;
			SignalVal_au8[2] = 0;
			SignalVal_au8[3] = 0;
			SignalVal_au8[4] = 0;
			
		}
		
	}
	
/*					user code end		*/

//	SignalVal_au8[0] = 'H'; 	/*To Display 'H'*/
//	SignalVal_au8[1] = 'C'; 	/*To Display 'C'*/
	
	for(i=CLUSTER_SIG_START_E; i < TOTAL_SIGNALS_E; i++)
	{
		if(ClusterSigConf_aSt[i].ClusterSignals_En == ClusterSignals_En)
		{
			for(j=0; j < ClusterSigConf_aSt[i].SigLength_u8; j++)
			{
				for(k=0; k < ClusterSigConf_aSt[i].SignalsValue_pSt[j].SignalLen_En; k++)
				{
					if(SignalVal_au8[j] == ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SignalsValue_En)
					{
						for(l=0; l < ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].Seg_Count_u8; l++)
						{
							REG_WRITE(i,j,k,l);
						}
					}	
				}			
			}
			
		break;
		}
	}

	return;	
}	


/********************************************************EOF***********************************************************/