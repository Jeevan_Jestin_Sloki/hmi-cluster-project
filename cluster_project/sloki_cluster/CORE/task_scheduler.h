#ifndef TASK_SCHEDULER_H
#define TASK_SCHEDULER_H

#include"r_cg_macrodriver.h"
#include "Cluster_Conf.h"

extern bool task_scheduler_exit_b;
extern bool StartLCD_b;

extern void task_scheduler_start(void);
extern void task_scheduler_stop(void);

void TurnOnLCD(void);


#endif
