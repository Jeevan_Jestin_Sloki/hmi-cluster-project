#include"task_scheduler.h"
#include "r_cg_lcd.h"
#include "r_cg_timer.h"
#include "Cluster_test.h"


#include "SegDispWrite.h"
#include "timer_user.h"

bool task_scheduler_exit_b=FALSE;
bool StartLCD_b = TRUE;



void task_scheduler_start(void)
{
	while (!task_scheduler_exit_b)
	{
        	if(StartLCD_b)
		{
			StartLCD_b = FALSE;
			TurnOnLCD();
		}
		
		if(_500mSecFlag_b == TRUE)
		{
			TestMain();
			_500mSecFlag_b = FALSE;
		}
    	}
	
}


void TurnOnLCD(void)
{
	/*Turn on backlight*/
	
	TDR24 = (uint16_t)(50*320);
	
	R_LCD_VoltageOn();
	R_LCD_Start();
	
	
	return;	
}

void TurnOFFLCD(void)
{
	R_LCD_Stop();
	R_LCD_VoltageOff();
	
	/*Turn off backlight*/
	TDR24 = (uint16_t)(0*320);
	return;	
}



