#include "board_init.h"
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_timer.h"
#include "r_cg_userdefine.h"
#include "r_cg_lcd.h"

void Hardware_Init(void)
{
	DI();
	R_CGC_Get_ResetSource();
	R_CGC_Create();
    	R_TAU0_Create();
	R_TAU2_Create();
	R_LCD_Create();
    	IAWCTL = 0x00U;
    	GUARD = 0x00U;
	EI();
	return;
}


void Software_Ini(void)
{
	R_TAU0_Channel0_Start();
	R_TAU2_Channel0_Start();
	return;
}

